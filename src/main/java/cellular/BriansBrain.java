package cellular;
public class BriansBrain extends GameOfLife {

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    

    @Override
	public CellState getNextCell(int row, int col) {
		if (getCellState(row, col) == CellState.ALIVE){
			return CellState.DYING;
		}

		else if (getCellState(row, col) == CellState.DYING){
			return CellState.DEAD;
		}
		else if (getCellState(row, col) == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) == 2){
	    	return CellState.ALIVE;
		}

		else {
			return CellState.DEAD;
		
		}
	
	}
    
}
