package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int columns;
    private int rows;
    private CellState[][] cells;

   

    


    public CellGrid(int rows, int columns, CellState initialState) {
        this.columns = columns;
		this.rows = rows;
        cells = new CellState[rows][columns];
        for ( int i=0; i<rows; i++){
            for (int j=0; j<columns; j++){
                this.cells[i][j]=initialState;

            }
        }
	
	}

    @Override
    public int numRows() {
        
      
        return rows;
    }


    @Override
    public int numColumns() {
    
        return columns;
    }

    

    @Override
    public void set(int row, int column, CellState element) {
    if (row < 0 || row>numRows())
        throw new IndexOutOfBoundsException("Ikke gyldig");

    if (column < 0 || column>numColumns())
        throw new IndexOutOfBoundsException("Ikke gyldig");

    this.cells[row][column]=element; 
    }

     
	  // Get the contents of the cell in the given row,column location.
	  
	 // row must be greater than or equal to 0 and less than numRows().
	 // column must be greater than or equal to 0 and less than numColumns().
	 
	  //@param column The column of the cell to get the contents of.
	 // @param row    The row of the cell to get contents of.

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row>numRows())
        throw new IndexOutOfBoundsException("Ikke gyldig");

        if (column < 0 || column>numColumns())
        throw new IndexOutOfBoundsException("Ikke gyldig");
        
        return cells[row][column];
        
    }

    @Override
    public IGrid copy() {
        CellGrid kopi = new CellGrid(numRows(), numColumns(), CellState.DEAD);
    
    

        for ( int i=0; i<rows; i++){
            for (int j=0; j<columns; j++){
                kopi.set(i, j, get(i, j));
            }
        }

        return kopi;
    }


}
    
        
